DESCRIPTION:
------------
Instead of showing a standard "404 Page not found", this module
shows a childfocus missing poster.

INSTALLATION:
-------------
1. Extract the tar.gz into your 'modules' or directory.
2. Enable the module on the modules page.
3. Your 404 page will be automatically replaced with a "not_found"

CONFIGURATION
-------------
1. Visit 'admin >> configuration >> site-information'

UNINSTALLTION:
--------------
1. Disable the module.
2. Uninstall the module, which will blank the the 404 page
